#version 330

layout(triangles) in;
layout(line_strip, max_vertices = 6) out;

in VS_OUT {
	vec3 bitangentCamSpace;
	vec4 gl_Position;
} vsout[];

uniform mat4 projectionMatrix; //из системы координат камеры в усеченные координаты

const float MAGNITUDE = 0.1;

void GenerateLine(int index)
{
    gl_Position = vsout[index].gl_Position;
    EmitVertex();
    gl_Position = vsout[index].gl_Position + projectionMatrix * vec4(vsout[index].bitangentCamSpace, 0.0) * MAGNITUDE;
    EmitVertex();
    EndPrimitive();
}

void main()
{
    GenerateLine(0); // вектор нормали для первой вершины
    GenerateLine(1); // ... для второй
    GenerateLine(2); // ... для третьей
}
