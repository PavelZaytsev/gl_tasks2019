/**
Преобразует координаты вершины и нормально в систему координат виртуальной камеры и передает на выход.
Копирует на выход текстурные координаты.
*/

#version 330

out VS_OUT {
	vec3 tangentCamSpace;
	vec4 gl_Position;
} vsout;

//стандартные матрицы для преобразования координат
uniform mat4 modelMatrix; //из локальной в мировую
uniform mat4 viewMatrix; //из мировой в систему координат камеры
uniform mat4 projectionMatrix; //из системы координат камеры в усеченные координаты

//матрица для преобразования нормалей из локальной системы координат в систему координат камеры
uniform mat3 normalToCameraMatrix;


layout(location = 0) in vec3 vertexPosition; //координаты вершины в локальной системе координат
layout(location = 3) in vec3 vertexTangent; //тангент в локальной системе координат

void main()
{
	vsout.tangentCamSpace = normalize(normalToCameraMatrix * vertexTangent);
	gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(vertexPosition, 1.0);
	vsout.gl_Position = gl_Position;
}
