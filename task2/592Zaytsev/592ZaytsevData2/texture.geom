#version 330

layout(triangles) in;
layout(line_strip, max_vertices = 6) out;

in VS_OUT {
	vec3 normalCamSpace; //нормаль в системе координат камеры (интерполирована между вершинами треугольника)
//	vec4 lightPosCamSpace; //положение источника света в системе координат камеры (интерполировано между вершинами треугольника)
//	vec4 posCamSpace; //координаты вершины в системе координат камеры (интерполированы между вершинами треугольника)
//	vec2 texCoord; //текстурные координаты (интерполирована между вершинами треугольника)
//	vec3 tangentCamSpace;
//	vec3 bitangentCamSpace;
} vsout[];


//out vec3 normalCamSpace; //нормаль в системе координат камеры
//out vec4 lightPosCamSpace; //положение источника света в системе координат камеры
//out vec4 posCamSpace; //координаты вершины в системе координат камеры
//out vec2 texCoord; //текстурные координаты
//out vec3 tangentCamSpace;
//out vec3 bitangentCamSpace;

const float MAGNITUDE = 0.4;

void GenerateLine(int index)
{
    gl_Position = gl_in[index].gl_Position;
    EmitVertex();
    gl_Position = gl_in[index].gl_Position + vec4(gs_in[index].normal, 0.0) * MAGNITUDE;
    EmitVertex();
    EndPrimitive();
}

void main()
{
	normalCamSpace = vsout[0].normalCamSpace; 
	lightPosCamSpace = vsout[0].lightPosCamSpace;
	posCamSpace = vsout[0].posCamSpace;
	texCoord = vsout[0].texCoord;
	tangentCamSpace = vsout[0].tangentCamSpace;
	bitangentCamSpace = vsout[0].bitangentCamSpace;
	EmitVertex();
    EndPrimitive();
}
