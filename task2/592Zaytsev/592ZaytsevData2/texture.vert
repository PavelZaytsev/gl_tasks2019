/**
Преобразует координаты вершины и нормально в систему координат виртуальной камеры и передает на выход.
Копирует на выход текстурные координаты.
*/

#version 330

out vec3 normalCamSpace;
out	vec4 lightPosCamSpace; //положение источника света в системе координат камеры (интерполировано между вершинами треугольника)
out	vec4 posCamSpace; //координаты вершины в системе координат камеры (интерполированы между вершинами треугольника)
out	vec2 texCoord; //текстурные координаты (интерполирована между вершинами треугольника)
out	vec3 tangentCamSpace;
out	vec3 bitangentCamSpace;

//стандартные матрицы для преобразования координат
uniform mat4 modelMatrix; //из локальной в мировую
uniform mat4 viewMatrix; //из мировой в систему координат камеры
uniform mat4 projectionMatrix; //из системы координат камеры в усеченные координаты

//матрица для преобразования нормалей из локальной системы координат в систему координат камеры
uniform mat3 normalToCameraMatrix;

struct LightInfo
{
    vec3 pos; //положение источника света в мировой системе координат (для точечного источника)
    vec3 ambient;
    vec3 color;
};
uniform LightInfo light;

layout(location = 0) in vec3 vertexPosition; //координаты вершины в локальной системе координат
layout(location = 1) in vec3 vertexNormal; //нормаль в локальной системе координат
layout(location = 2) in vec2 vertexTexCoord; //текстурные координаты вершины
layout(location = 3) in vec3 vertexTangent; //тангент в локальной системе координат
layout(location = 4) in vec3 vertexBitangent; //битангент в локальной системе координат

void main()
{
	texCoord = vertexTexCoord;

	posCamSpace = viewMatrix * modelMatrix * vec4(vertexPosition, 1.0); //преобразование координат вершины в систему координат камеры
	normalCamSpace = normalize(normalToCameraMatrix * vertexNormal); //преобразование нормали в систему координат камеры
	tangentCamSpace = normalize(normalToCameraMatrix * vertexTangent);
	bitangentCamSpace = normalize(normalToCameraMatrix * vertexBitangent);

	lightPosCamSpace = viewMatrix * vec4(light.pos, 1.0); //преобразование положения источника света в систему координат камеры

	//normalCamSpace = -1*normalize((projectionMatrix*vec4(normalCamSpace, 0.0)).xyz);
	//tangentCamSpace = normalize((projectionMatrix*vec4(tangentCamSpace, 0.0)).xyz);
	//bitangentCamSpace = normalize((projectionMatrix*vec4(bitangentCamSpace, 0.0)).xyz);
	//lightPosCamSpace = projectionMatrix*lightPosCamSpace;
	//posCamSpace = projectionMatrix*posCamSpace;


	gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(vertexPosition, 1.0);
}
