#include <Application.hpp>
#include <LightInfo.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include <Texture.hpp>
#include <glm/gtc/quaternion.hpp>

#include <iostream>
#include <sstream>
#include <vector>
#include <deque>

/**
Пример с текстурированием разных 3д-моделей
*/
class SampleApplication : public Application
{
public:
	MeshPtr _cube;
	MeshPtr _sphere;
	MeshPtr _bunny;
	MeshPtr _myModel;
	MeshPtr _modelForNormals;

	MeshPtr _marker; //Маркер для источника света

	bool UseNormalMapping = false;
	bool ShowModel = true;
	bool ShowNormals = false;
	bool ShowTangents = false;
	bool ShowBitangents = false;

	//Идентификатор шейдерной программы
	ShaderProgramPtr _shader;
	ShaderProgramPtr _normalDisplayShader;
	ShaderProgramPtr _bDisplayShader;
	ShaderProgramPtr _tDisplayShader;
	ShaderProgramPtr _markerShader;

	//Переменные для управления положением одного источника света
	float _lr = 3.0;
	float _phi = 0.0;
	float _theta = glm::pi<float>() * 0.25f;

	//Параметры источника света
	glm::vec3 _lightAmbientColor;
	glm::vec3 _lightColor;

	//Параметры материала
	glm::vec3 _bunnyDiffuseColor;
	glm::vec3 _bunnySpecularColor;
	float _diffuseFraction = 0.0f;
	float _roughnessValue = 0.3f;
	float _F0 = 0.8f;

	LightInfo _light;

	TexturePtr _myTexture;
	TexturePtr _normalMap;


	GLuint _sampler;

	void makeScene() override
	{
		Application::makeScene();

		//=========================================================
		//Создание и загрузка мешей		

		//_cube = makeCube(0.5f);
		//_cube->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.5f)));

		//_sphere = makeSphere(0.5f);
		//_sphere->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.5f)));

		//_bunny = loadFromFile("models/bunny.obj");
		//_bunny->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 1.0f, 0.0f)));

		_myModel = makeMyModel(1.0f, 1.5f, 0.0f);
		_myModel->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, -0.5f)));
		_modelForNormals = makeMyModel(1.0f, 1.5f, 0.0f, 20);
		_modelForNormals->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, -0.5f)));

		_marker = makeSphere(0.1f);

		//=========================================================
		//Инициализация шейдеров

		//_shader = std::make_shared<ShaderProgram>("592ZaytsevData2/texture.vert", "592ZaytsevData2/texture.geom", "592ZaytsevData2/texture.frag");
		_shader = std::make_shared<ShaderProgram>("592ZaytsevData2/texture.vert", "592ZaytsevData2/texture.frag");
		_normalDisplayShader = std::make_shared<ShaderProgram>("592ZaytsevData2/textureNormals.vert", "592ZaytsevData2/textureNormals.geom", "592ZaytsevData2/textureNormals.frag");
		_bDisplayShader = std::make_shared<ShaderProgram>("592ZaytsevData2/textureB.vert", "592ZaytsevData2/textureB.geom", "592ZaytsevData2/textureB.frag");
		_tDisplayShader = std::make_shared<ShaderProgram>("592ZaytsevData2/textureT.vert", "592ZaytsevData2/textureT.geom", "592ZaytsevData2/textureT.frag");
		_markerShader = std::make_shared<ShaderProgram>("592ZaytsevData2/marker.vert", "592ZaytsevData2/marker.frag");

		//=========================================================
		//Инициализация значений переменных освщения
		_lightAmbientColor = glm::vec3(0.5, 0.5, 0.5);
		_lightColor = glm::vec3(1.0, 1.0, 1.0);

		//Инициализация материала
		_bunnyDiffuseColor = glm::vec3(1.0, 1.0, 0.0);
		_bunnySpecularColor = glm::vec3(1.0, 1.0, 1.0);
		_diffuseFraction = 0.0f;
		_roughnessValue = 0.3f;
		_F0 = 0.8f;

		//=========================================================
		//Загрузка и создание текстур
		_myTexture = loadTexture("592ZaytsevData2/Tileable_stone_paving_Texture.png");
		_normalMap = loadTexture("592ZaytsevData2/Tileable_stone_paving_Texture_NORMAL.png");

		//=========================================================
		//Инициализация сэмплера, объекта, который хранит параметры чтения из текстуры
		glGenSamplers(1, &_sampler);
		glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);
	}

	void updateGUI() override
	{
		Application::updateGUI();

		ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
		if (ImGui::Begin("MIPT OpenGL task2 try1", NULL, ImGuiWindowFlags_AlwaysAutoResize))
		{
			ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

			if (ImGui::CollapsingHeader("Light"))
			{
				ImGui::ColorEdit3("ambient", glm::value_ptr(_lightAmbientColor));
				ImGui::ColorEdit3("color", glm::value_ptr(_lightColor));

				ImGui::SliderFloat("radius", &_lr, 0.1f, 10.0f);
				ImGui::SliderFloat("phi", &_phi, 0.0f, 2.0f * glm::pi<float>());
				ImGui::SliderFloat("theta", &_theta, 0.0f, glm::pi<float>());
			}
			if (ImGui::CollapsingHeader("material"))
			{
				ImGui::ColorEdit3("mat diffuse", glm::value_ptr(_bunnyDiffuseColor));
				ImGui::ColorEdit3("mat specular", glm::value_ptr(_bunnySpecularColor));

				ImGui::SliderFloat("diffuse fraction", &_diffuseFraction, 0.01f, 1.0f);
				ImGui::SliderFloat("roughness", &_roughnessValue, 0.01f, 1.0f);
				ImGui::SliderFloat("F0", &_F0, 0.01f, 1.0f);
				
				ImGui::Checkbox("Use normal mapping", &UseNormalMapping);
				ImGui::Checkbox("Show model", &ShowModel);
				ImGui::Checkbox("Show normals", &ShowNormals);
				ImGui::Checkbox("Show tangents", &ShowTangents);
				ImGui::Checkbox("Show bitangents", &ShowBitangents);
			}
		}
		ImGui::End();
	}

	void draw() override
	{
		//Получаем текущие размеры экрана и выставлям вьюпорт
		int width, height;
		glfwGetFramebufferSize(_window, &width, &height);

		glViewport(0, 0, width, height);

		//Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		//Подключаем шейдер		
		_shader->use();

		//Загружаем на видеокарту значения юниформ-переменных
		_shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
		_shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

		_light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
		glm::vec3 lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 1.0));

		//_shader->setVec3Uniform("light.pos", lightPosCamSpace); //копируем положение уже в системе виртуальной камеры
		_shader->setVec3Uniform("light.pos", _light.position);
		_shader->setVec3Uniform("light.ambient", _lightAmbientColor);
		_shader->setVec3Uniform("light.color", _lightColor);

		GLuint textureUnitForDiffuseTex = 0;

		glBindSampler(textureUnitForDiffuseTex, _sampler);
		glActiveTexture(GL_TEXTURE0 + textureUnitForDiffuseTex);  //текстурный юнит 0
		_myTexture->bind();
		_shader->setIntUniform("diffuseTex", textureUnitForDiffuseTex);
		textureUnitForDiffuseTex += 1;

		glBindSampler(textureUnitForDiffuseTex, _sampler);
		glActiveTexture(GL_TEXTURE0 + textureUnitForDiffuseTex);  //текстурный юнит 1
		_normalMap->bind();
		_shader->setIntUniform("normalMap", textureUnitForDiffuseTex);

		//Загружаем на видеокарту матрицы модели мешей и запускаем отрисовку
		//{
		//	_shader->setMat4Uniform("modelMatrix", _cube->modelMatrix());
		//	_shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _cube->modelMatrix()))));

		//	_cube->draw();
		//}

		//{
		//	_shader->setMat4Uniform("modelMatrix", _sphere->modelMatrix());
		//	_shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _sphere->modelMatrix()))));

		//	_sphere->draw();
		//}

		//{
		//	_shader->setMat4Uniform("modelMatrix", _bunny->modelMatrix());
		//	_shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _bunny->modelMatrix()))));

		//	_bunny->draw();
		//}

		{

				_shader->setMat4Uniform("modelMatrix", _myModel->modelMatrix());
				_shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _myModel->modelMatrix()))));
				//_shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(_camera.viewMatrix * _myModel->modelMatrix()));

				_shader->setVec3Uniform("material.Kd", glm::vec3(0.98, 0.82, 0.75));
				_shader->setVec3Uniform("material.Ks", glm::vec3(0.98, 0.82, 0.75));
				_shader->setFloatUniform("material.diffuseFraction", _diffuseFraction);
				_shader->setFloatUniform("material.roughnessValue", _roughnessValue);
				_shader->setFloatUniform("material.F0", _F0);
				if (UseNormalMapping)
				{
					_shader->setIntUniform("UseNormalMapping", 1);
				}
				else
				{
					_shader->setIntUniform("UseNormalMapping", 0);
				}

				if (ShowModel)
				{
					_myModel->draw();
				}

				//glClear(GL_DEPTH_BUFFER_BIT);

				//_normalDisplayShader
				_normalDisplayShader->use();
				_normalDisplayShader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
				_normalDisplayShader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
				_normalDisplayShader->setMat4Uniform("modelMatrix", _modelForNormals->modelMatrix());
				_normalDisplayShader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _myModel->modelMatrix()))));
				if (ShowNormals)
				{
					_modelForNormals->draw();
				}
				_bDisplayShader->use();
				_bDisplayShader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
				_bDisplayShader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
				_bDisplayShader->setMat4Uniform("modelMatrix", _modelForNormals->modelMatrix());
				_bDisplayShader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _myModel->modelMatrix()))));
				if (ShowBitangents)
				{
					_modelForNormals->draw();
				}
				_tDisplayShader->use();
				_tDisplayShader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
				_tDisplayShader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
				_tDisplayShader->setMat4Uniform("modelMatrix", _modelForNormals->modelMatrix());
				_tDisplayShader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _myModel->modelMatrix()))));
				if (ShowTangents)
				{
					_modelForNormals->draw();
				}

		}

		//Рисуем маркеры для всех источников света		
		{
			_markerShader->use();

			_markerShader->setMat4Uniform("mvpMatrix", _camera.projMatrix * _camera.viewMatrix * glm::translate(glm::mat4(1.0f), _light.position));
			_markerShader->setVec4Uniform("color", glm::vec4(glm::vec3(0.8, 0.8, 0.8), 1.0f));
			_marker->draw();
		}

		//Отсоединяем сэмплер и шейдерную программу
		glBindSampler(0, 0);
		glUseProgram(0);
	}
};

int main()
{
	SampleApplication app;
	app.start();

	return 0;
}